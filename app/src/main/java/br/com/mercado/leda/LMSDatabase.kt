package br.com.mercado.leda

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [
    Pais::class
], version = 1)

abstract class LMSDatabase: RoomDatabase() {
    abstract fun paisDAO(): PaisDAO
}