package br.com.mercado.leda
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import br.com.mercado.leda.databinding.ActivityCadastroBinding
import br.com.mercado.leda.databinding.ActivityTelaInicialBinding
import com.google.gson.Gson

class TelaCadastroActivity : DebugActivity() {

    private val binding by lazy {
        ActivityCadastroBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarInclude.toolbar)

        binding.buttonSave.setOnClickListener {
            val p = Pais()
            p.nome = binding.editPais.text.toString()
            p.capial = binding.editCapital.text.toString()
            p.populacao = binding.editPopu.text.toString()
            p.continente = binding.editContinente.text.toString()
            p.bandeira = binding.editURL.text.toString()
            p.latitude = binding.editLatitu.text.toString()
            p.longitute = binding.editLongitu.text.toString()

            Thread {
                PaisService.savePais(p)
                runOnUiThread {
                    finish()
                }
            }.start()
        }

        supportActionBar?.title = "Cadastro"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_cadastro, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item?.itemId

        if (id == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}

