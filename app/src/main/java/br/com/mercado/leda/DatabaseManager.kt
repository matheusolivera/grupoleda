package br.com.mercado.leda

import androidx.room.Room

object DatabaseManager {

    private var dbInstance: LMSDatabase

    init {
        val contexto = LMSApplication.getInstance().applicationContext
        dbInstance = Room.databaseBuilder(
            contexto,
            LMSDatabase::class.java,
            "pais.sqlite"
        ).build()
    }

    fun getPaisDAO(): PaisDAO {
        return dbInstance.paisDAO()
    }
}