package br.com.mercado.leda

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import br.com.mercado.leda.databinding.ActivityPaisBinding
import com.squareup.picasso.Picasso

class PaisActivity : AppCompatActivity() {
    private val binding by lazy {
        ActivityPaisBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarInclude.toolbar)
        var p = intent.extras?.getSerializable("pais") as Pais

        binding.btnMap.setOnClickListener{
            var it = Intent(this, MapsActivity::class.java)
            it.putExtra("pais", p)
            startActivity(it)
        }

        supportActionBar?.title = "Informações do País:"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        binding.id.text = p.id.toString()
        binding.nomePais.text = p.nome
        binding.nomeCapital.text = p.capial
        binding.qtdPopulacao.text = p.populacao
        binding.nomeContinente.text = p.continente

        Picasso.with(this).load(p.bandeira).into(binding.bandeira)

        binding.btnDelete.setOnClickListener{

            Thread {
                PaisService.deletePais(binding.id.text.toString())
                runOnUiThread {
                    finish()
                }
            }.start()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_cadastro, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item?.itemId

        if (id == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}