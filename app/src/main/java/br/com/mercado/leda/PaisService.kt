package br.com.mercado.leda

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.net.URL

object PaisService {

    val host = "https://leda-paises.herokuapp.com"
    val TAG = "WS_LMSApp"

    fun getPais(): List<Pais> {

        try {
            val url = "$host/paises"
            val json = HttpHelper.get(url)

            var paises = parseJson<MutableList<Pais>>(json)

            for (item in paises){
                val p = Pais()
                p.capial = item.capial
                p.latitude = item.latitude
                p.populacao = item.populacao
                p.continente = item.continente
                p.nome = item.nome
                p.bandeira = item.bandeira
                p.longitute = item.longitute

                // DatabaseManager.getPaisDAO().insert(p)
            }

            return paises

        } catch (ex: Exception) {
            var paises = DatabaseManager.getPaisDAO().findAll()
            return paises
        }
    }

    fun savePais(pais: Pais) {
        try {
            var json = GsonBuilder().create().toJson(pais)
            HttpHelper.post("$host/paises", json)
        } catch (ex: Exception) {
            DatabaseManager.getPaisDAO().insert(pais)
        }

    }

    fun deletePais(id: String) {
        try {
            HttpHelper.delete("$host/paises/" + id)
        } catch (ex: Exception) {
            "DatabaseManager.getPaisDAO().insert(pais)"
        }

    }

    inline fun <reified T> parseJson(json: String): T {
        val type = object : TypeToken<T>(){}.type
        return Gson().fromJson<T>(json, type)
    }
}