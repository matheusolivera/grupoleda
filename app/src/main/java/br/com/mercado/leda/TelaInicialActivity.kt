package br.com.mercado.leda
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.mercado.leda.databinding.ActivityTelaInicialBinding
import com.google.android.material.navigation.NavigationView


class TelaInicialActivity : DebugActivity(),
    NavigationView.OnNavigationItemSelectedListener {

    private val binding by lazy {
        ActivityTelaInicialBinding.inflate(layoutInflater)
    }

    private var paises = listOf<Pais>()

    private val context: Context get() = this
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val args = intent.extras

        val nome = args?.getString("nome")



        Toast.makeText(context, "Login: $nome", Toast.LENGTH_LONG).show()


        setSupportActionBar(binding.toolbarInclude.toolbar)

        supportActionBar?.title = "Bandeiras"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.btnSair.setOnClickListener {cliqueSair()}

        configuraMenuLateral()

        binding.recyclerPaises?.layoutManager = LinearLayoutManager(this)
        binding.recyclerPaises?.setHasFixedSize(true)
    }

    override fun onResume() {
        super.onResume()
        this.taskPaises()
    }

    private fun taskPaises(){
        Thread {
            paises = PaisService.getPais()
            runOnUiThread {
                binding.recyclerPaises?.adapter = PaisAdapter(paises) {
                    onClickPais(it)
                }

                val intent = Intent(this, PaisActivity::class.java)
                intent.putExtra("paises", paises[2])
                NotificationUtil.create(1, intent, "País", "Bem-Vindo a Lista de Países")
            }
        }.start()
    }

    fun onClickPais(pais: Pais) {
        Toast.makeText(this, "Clicou no país", Toast.LENGTH_LONG).show()

        var it = Intent(this, PaisActivity::class.java)
        it.putExtra("pais", pais)
        startActivity(it)
    }

    fun cliqueSair() {
        val returnIntent = Intent();
        returnIntent.putExtra("result","Saída Bandeira");
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_main, menu)

        (menu?.findItem(R.id.action_buscar)?.actionView as SearchView).setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                Toast.makeText(context, "Digitando: $newText", Toast.LENGTH_LONG).show()
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Toast.makeText(context, "Buscar: $query", Toast.LENGTH_LONG).show()
                return false
            }

        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item?.itemId
        if (id == R.id.action_buscar) {
            Toast.makeText(this, "Clicou buscar", Toast.LENGTH_LONG).show()
        }
        else if (id == R.id.action_atualizar) {
            binding.progressAtualizar.visibility = View.VISIBLE
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    binding.progressAtualizar.visibility = View.GONE
                },
                10000
            )
        }
        else if (id == R.id.action_adicionar) {

            val intent = Intent(context, TelaCadastroActivity::class.java)

            startActivity(intent)
        }
        else if (id == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_paises -> {
                Toast.makeText(this, "Clicou em Lista de Países", Toast.LENGTH_LONG).show()
                val intent = Intent(context, TelaInicialActivity::class.java)

                val params = Bundle()
                intent.putExtras(params)

                startActivityForResult(intent, 1)
            }

            R.id.nav_sobre -> {
                Toast.makeText(this, "Clicou em Sobre", Toast.LENGTH_LONG).show()
                val intent = Intent(context, TelaSobreActivity::class.java)

                startActivity(intent)

            }

            R.id.nav_sair -> {
                Toast.makeText(this, "Clicou em Sair", Toast.LENGTH_LONG).show()
                val intent = Intent(context, MainActivity::class.java)

                val params = Bundle()
                intent.putExtras(params)

                startActivityForResult(intent, 1)
            }
        }

        binding.layoutMenuLateral.closeDrawer(GravityCompat.START)

        return true
    }

    private fun configuraMenuLateral() {
        var toggle = ActionBarDrawerToggle(
            this,
            binding.layoutMenuLateral,
            binding.toolbarInclude.toolbar,
            R.string.abrir,
            R.string.fechar
        )
        binding.layoutMenuLateral.addDrawerListener(toggle)
        toggle.syncState()

        binding.menuLateral.setNavigationItemSelectedListener(this)
    }
}
