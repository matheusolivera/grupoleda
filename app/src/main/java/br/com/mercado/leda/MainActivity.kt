package br.com.mercado.leda

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.*
import br.com.mercado.leda.databinding.LoginBinding

class MainActivity : DebugActivity() {

    private val binding by lazy {
        LoginBinding.inflate(layoutInflater)
    }

    private val context: Context get() = this
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val nomeUsuario = Prefs.getString("nome_usuario")
        val senhaUsuario = Prefs.getString("senha_usuario")
        val checkLogin = Prefs.getBoolean("checkLogin")
        if (nomeUsuario.equals("")) {
            binding.msgLogin.text = "Olá Aluno!!!"
        } else {
            binding.msgLogin.text = "Bem-vindo de volta $nomeUsuario"
        }

        binding.userPlace.setText(nomeUsuario)
        binding.passwordPlace.setText(senhaUsuario)
        binding.checkBoxLogin.isChecked = checkLogin


        binding.imgPlace.setImageResource(R.drawable.mapa)

        binding.btnLogin.setOnClickListener {onClickLogin() }

    }

    @SuppressLint("SuspiciousIndentation")
    fun onClickLogin(){

        val params = Bundle()
        params.putString("nome", "aluno")
        params.putInt("numero", 15)
        intent.putExtras(params)

        val nome_usuario = binding.userPlace.text.toString()
        val senha_usuario = binding.passwordPlace.text.toString()

        val checkLogin = binding.checkBoxLogin.isChecked
        if (checkLogin) {
            Prefs.setString("nome_usuario", nome_usuario)
            Prefs.setString("senha_usuario", senha_usuario)
        } else {
            Prefs.setString("nome_usuario", "")
            Prefs.setString("senha_usuario", "")
        }

        Prefs.setBoolean("checkLogin", checkLogin)

        val valorUsuario = binding.userPlace.text.toString()

        val valorSenha = binding.passwordPlace.text.toString()

            if (valorUsuario == "aluno" && valorSenha == "impacta"){

                val intent = Intent(context, TelaInicialActivity::class.java)

                params.putString("nome", "$valorUsuario")
                intent.putExtras(params)


                startActivityForResult(intent, 1)
            }
        else{
                Toast.makeText(context, "Usuário ou Senha incorretos", Toast.LENGTH_LONG).show()
            }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            val result = data?.getStringExtra("result")
            Toast.makeText(context, "$result", Toast.LENGTH_LONG).show()
        }
    }
}
